FROM python:3.12-slim
ARG UID=1000
ARG GID=1000
RUN apt-get update && apt-get install -y \
     gcc \
     libpq-dev \
 && rm -rf /var/lib/apt/lists/*
RUN groupadd -g "${GID}" python \
  && useradd --create-home --no-log-init -u "${UID}" -g "${GID}" python
USER python
WORKDIR /home/python/app
COPY ./app/requirements.txt .
RUN pip3 install --user --no-cache-dir -r requirements.txt
COPY --chown=python:python ./app .
CMD [ "python3", "bot.py" ]
