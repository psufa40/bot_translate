import sqlite3
import os
import random
import json
class SQLiteHandler:
    def __init__(self, database_name='bot_translate.db'):
        self.database_name = database_name
        self.connection = None
        self.cursor = None
        
    def connect(self):
        try:
            self.connection = sqlite3.connect(self.database_name)
            self.cursor = self.connection.cursor()
        except sqlite3.Error as e:
            print(e),

    def disconnect(self):
        if self.connection:
            self.connection.close()

    def create_database_if_not_exists(self):
        if not os.path.exists(self.database_name):
            self.connect()
            self.create_tables()
            self.disconnect()

    def create_tables(self):
        try:
            self.connect()
            self.cursor.execute('''
                CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    user_id INTEGER,
                    ❤️ TEXT,
                    📕 TEXT,
                    📗 TEXT,
                    📘 TEXT,
                    📙 TEXT
                )
            ''')
            
            # Создание таблицы
            self.cursor.execute('''
                CREATE TABLE IF NOT EXISTS words_en (
                    word_en_id INTEGER PRIMARY KEY AUTOINCREMENT,
                    word_en TEXT NOT NULL UNIQUE,
                    verb_ru TEXT,
                    noun_ru TEXT,
                    adverb_ru TEXT,
                    adjective_ru TEXT,
                    other_ru TEXT,
                    example_en TEXT,
                    example_ru TEXT
                )
            ''')
            self.connection.commit()
        except sqlite3.Error as e:
            print(e)
        finally:
            self.disconnect()

# Получение слов таблицы словаря 
    def get_word_from_db(self, word_en):
        try:
            self.connect()
            sql = "SELECT * FROM words_en WHERE word_en = ?"
            self.cursor.execute(sql, (word_en,))
            result = self.cursor.fetchone()
            return result  # Возвращаем найденную строку (или None, если не найдено)
    
        except sqlite3.Error as e:
            print(f"Error retrieving word from database: {e}")
            return None
        finally:
            self.disconnect()
#-------------- Add word in dictionary table -----------------------# 
    def add_word_to_words_table(self, word_en, verb_ru=None, noun_ru=None, adverb_ru=None, adjective_ru=None, other_ru=None, example_en=None, example_ru=None):
        try:
            self.connect()
            sql = '''
                INSERT INTO words_en (word_en, verb_ru, noun_ru, adverb_ru, adjective_ru, other_ru, example_en, example_ru)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)
            '''
            # Если переводы не найдены, передаем None или пустую строку
            data = (word_en, verb_ru or None, noun_ru or None, adverb_ru or None, adjective_ru or None, other_ru or None, example_en or None, example_ru or None)
            
            self.cursor.execute(sql, data)
            self.connection.commit()

        except sqlite3.IntegrityError as e:
            # Если слово уже существует (так как word_en уникально)
            print(f"Error: Word '{word_en}' already exists in the database.")
        except sqlite3.Error as e:
            print(f"Error adding word to words table: {e}")
        finally:
            self.disconnect()
#-------------- Add word to vip lists -----------------------# 
    def get_word_en_id(self, word_en):
        try:
            self.connect()
            sql = "SELECT word_en_id FROM words_en WHERE LOWER(word_en) = LOWER(?)"
            self.cursor.execute(sql, (word_en,))
            result = self.cursor.fetchone()
            return result[0] if result else None
        except sqlite3.Error as e:
            print(e)
        finally:
            self.disconnect()
            
    def update_user_list(self, user_id, word_en_id, list_name):
        try:
            self.connect()

            # Проверяем, есть ли пользователь в таблице
            sql_check_user = "SELECT id, {0} FROM users WHERE user_id = ?".format(list_name)
            self.cursor.execute(sql_check_user, (user_id,))
            user_entry = self.cursor.fetchone()

            if user_entry:
                # Пользователь найден
                user_list = user_entry[1]  # Список из указанного столбца (может быть None)
                if user_list:
                    words_list = json.loads(user_list)  # Преобразуем JSON в список
                else:
                    words_list = []

                # Добавляем или удаляем слово из списка
                if word_en_id in words_list:
                    words_list.remove(word_en_id)  # Удаляем слово из списка
                    action = "удалено"
                else:
                    words_list.append(word_en_id)  # Добавляем слово в список
                    action = "добавлено"

                # Сохраняем обновленный список в базу данных
                updated_list = json.dumps(words_list)
                sql_update_list = "UPDATE users SET {0} = ? WHERE user_id = ?".format(list_name)
                self.cursor.execute(sql_update_list, (updated_list, user_id))
            else:
                # Если пользователь не найден, создаем новую запись с первым словом
                words_list = [word_en_id]
                updated_list = json.dumps(words_list)
                sql_insert_user = "INSERT INTO users (user_id, {0}) VALUES (?, ?)".format(list_name)
                self.cursor.execute(sql_insert_user, (user_id, updated_list))
                action = "добавлено (новая запись)"

            self.connection.commit()
            return action  # Возвращаем результат действия
        except sqlite3.Error as e:
            print(f"Error updating list '{list_name}' for user {user_id}: {e}")
            return None
        finally:
            self.disconnect()
#-------------- get words for telegraph.py -----------------------# 
    def get_user_words(self, user_id, list_name):
        try:
            self.connect()
            # Получаем список слов для указанного пользователя и списка
            sql = f"SELECT {list_name} FROM users WHERE user_id = ?"
            self.cursor.execute(sql, (user_id,))
            result = self.cursor.fetchone()
            if not result or not result[0]:
                return []  # Возвращаем пустой список, если записи нет или список пуст

            word_ids = json.loads(result[0])  # Декодируем JSON в список ID слов
            if not word_ids:
                return []

            # Получаем слова из таблицы `words_en` по ID
            placeholders = ",".join("?" * len(word_ids))  # Создаем плейсхолдеры для IN
            sql_words = f"SELECT word_en_id, word_en, verb_ru, noun_ru, adverb_ru, adjective_ru, other_ru FROM words_en WHERE word_en_id IN ({placeholders})"
            self.cursor.execute(sql_words, word_ids)
            words_data = self.cursor.fetchall()

            # Формируем список кортежей (word_en_id, word_en, translations)
            result_words = []
            for row in words_data:
                word_en_id = row[0]
                word_en = row[1]
                translations = []
                for translation_group in row[2:]:
                    if translation_group:
                        translations.extend(translation_group.split(", ")[:2])  # Берем максимум 2 перевода из каждой группы
                translations_str = ", ".join(translations[:5])  # Ограничиваем до 5 переводов
                result_words.append((word_en_id, word_en, translations_str))
            return result_words
        except sqlite3.Error as e:
            print(f"Error retrieving words for user {user_id} list '{list_name}': {e}")
            return []
        finally:
            self.disconnect()
#-------------- get words for learning words -----------------------# 
    def learning_get_words(self, user_id, list_name, additional_count=3):
        try:
            # Получаем слова с переводами для пользователя
            word_ids_with_translations = self.get_user_words(user_id, list_name)
            
            if not word_ids_with_translations:
                return None, None

            # Присваиваем уникальные ID словам (используем уже существующие word_id из базы данных)
            enumerated_words = [(word_id, word, translations) 
                                for word_id, word, translations in word_ids_with_translations]
            
            # Выбираем случайное слово для перевода
            main_word = random.choice(enumerated_words)
            word_id, main_word_text, main_word_translations = main_word
            
            # Формируем строку с переводами
            main_translations_str = main_word_translations
            formatted_main_word = (word_id, main_word_text, main_translations_str)

            # Получаем дополнительные слова (исключаем выбранное основное слово)
            remaining_words = [word for word in enumerated_words if word[0] != word_id]
            additional_words = random.sample(remaining_words, min(additional_count, len(remaining_words)))
            formatted_additional_words = [(word_id, word_text) for word_id, word_text, _ in additional_words]
            return [formatted_main_word], formatted_additional_words

        except sqlite3.Error as e:
            print(e)
            return None, None
        finally:
            self.disconnect()
