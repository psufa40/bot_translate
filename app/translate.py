from textwrap import indent
import cloudscraper  # Импортируем cloudscraper
from bs4 import BeautifulSoup
from gtts import gTTS
from html import escape


class Translate:
    def __init__(self):
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36',
            "Content-Type": "application/json; charset=UTF-8"
        }
        self.scraper = cloudscraper.create_scraper()  # Инициализируем cloudscraper

    def extract_translations_by_classes(self, soup):
        # Определяем классы частей речи и их названия
        parts_of_speech = {
            "translation ltr dict n": "Noun",
            "translation ltr dict v": "Verb",
            "translation ltr dict adj": "Adjective",
            "translation ltr dict adv": "Adverb",
            "translation ltr dict no-pos": "Other"  # Для класса, где деление не нужно
        }

        # Словарь для хранения переводов
        translations = {}

        # Для каждого класса части речи
        for class_name, pos_name in parts_of_speech.items():
            # Находим все элементы для этого класса
            cards = soup.find_all(class_=class_name)
            words = []

            # Извлекаем переводы для найденных элементов
            for count, card in enumerate(cards):
                try:
                    translate_word = card.find(class_='display-term').get_text()
                    words.append(translate_word)
                    if class_name != "translation ltr dict no-pos" and count == 2:  # Ограничиваем до 3 переводов для всех классов, кроме "no-pos"
                        break
                except Exception as e:
                    print(f"Error extracting word for {pos_name}: {e}")

            # Если переводы найдены, добавляем в словарь
            if words:
                translations[pos_name] = words

        return translations

    def translate(self, word):
        url_for_translate = "https://context.reverso.net/translation/english-russian/"
        words = word.replace(" ", "+")
        if word:
            url_request_for_translate = url_for_translate + words
            print(f"Отправка запроса на: {url_request_for_translate}")  # Отладочный вывод
            try:
                # Используем cloudscraper вместо requests
                response = self.scraper.get(url=url_request_for_translate, headers=self.headers)
                print(f"Статус ответа: {response.status_code}")  # Отладочный вывод
                if response.status_code != 200:
                    print(f"Ошибка: Сервер вернул код {response.status_code}")
                    return [], "", ""

                soup = BeautifulSoup(response.text, 'lxml')
                print("HTML успешно получен и распарсен")  # Отладочный вывод
            except Exception as e:
                print(f"Ошибка при выполнении запроса: {e}")
                return [], "", ""

        # Извлекаем переводы, сгруппированные по частям речи
        translations = self.extract_translations_by_classes(soup)
        print(f"Извлечённые переводы: {translations}")  # Отладочный вывод

        # Теперь translate_words будет словарем, где ключи - части речи, а значения - списки переводов
        translate_words = []

        for pos_name, words in translations.items():
            # Вместо того чтобы добавлять в строку, создаем массив слов
            translate_words.append({
                'part_of_speech': pos_name,
                'translations': words
            })

        # Пример извлечения предложений
        sentences = soup.find_all('div', class_='example')
        sentense_en = sentense_ru = ""

        for count, card in enumerate(sentences):
            try:
                sentense_en = card.find(class_='src ltr').get_text()
                sentense_ru = card.find(class_='trg ltr').get_text()
                if count == 1:
                    break
            except Exception as e:
                print(f"Ошибка при извлечении предложений: {e}")

        print(f"Пример предложения (EN): {sentense_en}")  # Отладочный вывод
        print(f"Пример предложения (RU): {sentense_ru}")  # Отладочный вывод
        print(translate_words)

        return translate_words, sentense_en, sentense_ru

    # Генерация аудио для английского слова
    def get_audio(self, word):
        myobj = gTTS(text=word, lang="en", tld='ca', slow=False)
        myobj.save(word + ".mp3")
        return word + ".mp3"
