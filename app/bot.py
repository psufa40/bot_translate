from aiogram import Bot, Dispatcher, types,F
from aiogram.filters import Command
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, KeyboardButton, FSInputFile, ReplyKeyboardMarkup
from aiogram.fsm.storage.memory import MemoryStorage
from aiogram import Router
import os
import random
from dotenv import load_dotenv
from translate import Translate
from telegraph import TelegraphHandler
from database import SQLiteHandler
import asyncio

# Загрузка переменных окружения
load_dotenv()
API_TOKEN_TELEGRAM = os.getenv('API_TOKEN_TELEGRAM')

# Определение глобальных переменных
alphabet = {"а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", 
            "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я"}

Translate = Translate()
SQLiteHandler = SQLiteHandler()
TelegraphHandler = TelegraphHandler()

# Создание бота и диспетчера
bot = Bot(token=API_TOKEN_TELEGRAM)
storage = MemoryStorage()
dp = Dispatcher(bot=bot, storage=storage)

# ##################### Start Command ###########################

@dp.message(Command(commands="learning"))
async def start(message: types.Message):
    await message.delete()
    # Создаем инлайн-кнопки
    keyboard_learning = InlineKeyboardMarkup(inline_keyboard=[
        [
            InlineKeyboardButton(text="Learning ❤️", callback_data="learning_heart"),
            InlineKeyboardButton(text="Learning 📕", callback_data="learning_red_book"),
            InlineKeyboardButton(text="Learning 📗", callback_data="learning_green_book")
        ],
        [
            InlineKeyboardButton(text="Learning 📘", callback_data="learning_blue_book"),
            InlineKeyboardButton(text="Learning 📙", callback_data="learning_orange_book")
        ]
    ])

    await message.answer("Выбери список для изучения", reply_markup=keyboard_learning)

@dp.callback_query(F.data == "learning_heart")
async def words_learnings_return_heart(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await words_learnings_return_main(callback.message, user_id, "❤️")
    await callback.answer()

@dp.callback_query(F.data == "learning_red_book")
async def words_learnings_return_green_book(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await words_learnings_return_main(callback.message,  user_id,"📕")
    await callback.answer()

@dp.callback_query(F.data == "learning_green_book")
async def words_learnings_return_green_book(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await words_learnings_return_main(callback.message,  user_id,"📗")
    await callback.answer()

@dp.callback_query(F.data == "learning_blue_book")
async def words_learnings_return_blue_book(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await words_learnings_return_main(callback.message, user_id, "📘")
    await callback.answer()

@dp.callback_query(F.data == "learning_orange_book")
async def words_learnings_return_orange_book(callback: types.CallbackQuery):
    user_id = callback.from_user.id
    await words_learnings_return_main(callback.message, user_id, "📙")
    await callback.answer()

async def words_learnings_return_main(message: types.Message, icon: str):
    await message.answer(f"You chose {icon}!")

@dp.message(Command(commands="lists"))
async def show_lists(message: types.Message):
    await message.delete()
    # Получаем URL для списка слов
    url_vip_list, _ = TelegraphHandler.get_page_list(message.from_user.id, "❤️")
    url_learning_list, _ = TelegraphHandler.get_page_list(message.from_user.id, "📕")
    url_learning_list1, _ = TelegraphHandler.get_page_list(message.from_user.id, "📗")
    url_learning_list2, _ = TelegraphHandler.get_page_list(message.from_user.id, "📘")
    url_learning_list3, _ = TelegraphHandler.get_page_list(message.from_user.id, "📙")

    # Проверяем, чтобы URL не были пустыми
    default_url = 'https://telegra.ph/Bot-Translate-01-24'
    url_vip_list = url_vip_list or default_url
    url_learning_list = url_learning_list or default_url
    url_learning_list1 = url_learning_list1 or default_url
    url_learning_list2 = url_learning_list2 or default_url
    url_learning_list3 = url_learning_list3 or default_url

    # Создаем клавиатуру с кнопками
    keyboard_lists = InlineKeyboardMarkup(inline_keyboard=[
        [
            InlineKeyboardButton(text="❤️", url=url_vip_list),
            InlineKeyboardButton(text="📕", url=url_learning_list),
            InlineKeyboardButton(text="📗", url=url_learning_list1),
            InlineKeyboardButton(text="📘", url=url_learning_list2),
            InlineKeyboardButton(text="📙", url=url_learning_list3),
        ]
    ])

    # Отправляем сообщение с кнопками
    await message.answer("Показать избранный список слов:", reply_markup=keyboard_lists)

# ###################### Обработчик сообщений ECHO #####################
@dp.message()
async def echo_message(message: types.Message):
    user_id = message.from_user.id
    user_message = message.text
    check_en_ru = bool(alphabet.intersection(set(user_message.lower())))

    # Проверяем, есть ли слово в базе данных
    word_data = SQLiteHandler.get_word_from_db(user_message)

    if word_data:
        # Извлекаем данные из базы
        verb_ru = word_data[2] or ""
        noun_ru = word_data[3] or ""
        adverb_ru = word_data[4] or ""
        adjective_ru = word_data[5] or ""
        other_ru = word_data[6] or ""
        example_en = word_data[7] or ""
        example_ru = word_data[8] or ""

        # Формируем перевод
        translate_message = "\n".join(filter(None, [
            f"Verb: {verb_ru}" if verb_ru else "",
            f"Noun: {noun_ru}" if noun_ru else "",
            f"Adverb: {adverb_ru}" if adverb_ru else "",
            f"Adjective: {adjective_ru}" if adjective_ru else "",
            f"Other: {other_ru}" if other_ru else ""
        ]))

        # Отправляем данные из базы
        if translate_message:
            await message.answer(translate_message)
        if example_en:
            await message.answer(example_en)
        if example_ru:
            await message.answer(example_ru)
    else:
        # Выполняем перевод
        translate_words, sentense_en, sentense_ru = Translate.translate(user_message)

        # Инициализация данных для добавления в базу
        verb_ru = noun_ru = adverb_ru = adjective_ru = other_ru = ""
        for entry in translate_words:
            part_of_speech = entry['part_of_speech']
            translations = ", ".join(entry['translations'])

            if part_of_speech == "Verb":
                verb_ru = translations
            elif part_of_speech == "Noun":
                noun_ru = translations
            elif part_of_speech == "Adverb":
                adverb_ru = translations
            elif part_of_speech == "Adjective":
                adjective_ru = translations
            else:
                other_ru = translations

        example_en = sentense_en or ""
        example_ru = sentense_ru or ""

        # Добавляем слово в базу
        SQLiteHandler.add_word_to_words_table(user_message, verb_ru, noun_ru, adverb_ru, adjective_ru, other_ru, example_en, example_ru)

        # Преобразуем переводы в строку
        translate_message = "\n".join(filter(None, [
            f"{entry['part_of_speech']}: {', '.join(entry['translations'])}" for entry in translate_words
        ]))

        # Отправляем перевод
        await message.answer(translate_message)
        if sentense_en:
            await message.answer(sentense_en)
        if sentense_ru:
            await message.answer(sentense_ru)

    # Генерируем и отправляем аудио
    audio_words = user_message if not check_en_ru else (verb_ru if verb_ru else user_message)
    Translate.get_audio(audio_words)
    AUDIO_PATH = os.path.expanduser(audio_words + ".mp3")
    audio = FSInputFile(AUDIO_PATH)
    await message.answer_voice(audio)
    os.remove(audio_words + ".mp3")

    # Клавиатура с опциями для добавления в список изучаемых
    keyboard_word = InlineKeyboardMarkup(inline_keyboard=[[
        InlineKeyboardButton(text="❤️", callback_data=f"add_vip_list|{user_message}|{user_id}|❤️"),
        InlineKeyboardButton(text="📕", callback_data=f"add_vip_list|{user_message}|{user_id}|📕"),
        InlineKeyboardButton(text="📗", callback_data=f"add_vip_list|{user_message}|{user_id}|📗"),
        InlineKeyboardButton(text="📘", callback_data=f"add_vip_list|{user_message}|{user_id}|📘"),
        InlineKeyboardButton(text="📙", callback_data=f"add_vip_list|{user_message}|{user_id}|📙")
    ]])

    await message.answer("Вы можете добавить слово в список изучаемых:", reply_markup=keyboard_word)


# ################### For add word in list #####################

@dp.callback_query(lambda cb: cb.data.startswith("add_"))
async def handle_callback(callback_query: types.CallbackQuery):
    action, word, user_id, name_list = callback_query.data.split('|')
    user_id = int(user_id)  # Преобразуем user_id в int

    if action == "add_vip_list":
        await handle_learning_list(callback_query, word, user_id, name_list)

async def handle_learning_list(query: types.CallbackQuery, word_en: str, user_id: int, name_list: str):
    word_en_id = SQLiteHandler.get_word_en_id(word_en)
    if word_en_id is None:
        await query.answer(f"Слово '{word_en}' не найдено в базе данных.")
        return

    # Обновляем список пользователя
    action = SQLiteHandler.update_user_list(user_id, word_en_id, name_list)
    if action:
        await query.answer(f"Слово '{word_en}' {action} в {name_list} List.")
    else:
        await query.answer("Ошибка при обновлении списка.")
        return

    # Обновляем страницу на Telegraph
    TelegraphHandler.create_or_update_telegraph_page(user_id, name_list)

# Создаем роутер для управления callback_query
router = Router()

# ###################### Функция для возврата слов #####################
async def words_learnings_return_main(message: types.Message, user_id, name_list):
    await bot.delete_message(message.chat.id, message.message_id)

    # Получаем слова для обучения
    words, additional_words = SQLiteHandler.learning_get_words(user_id, name_list)

    # Проверяем наличие достаточного количества слов
    if not words or not additional_words or len(additional_words) < 3:
        await message.answer(f"Недостаточно слов в {name_list} List.")
        return

    # Создание списка кнопок, включая правильное слово и неправильные
    buttons = [
        [
            types.InlineKeyboardButton(
                text=words[0][1],  # Текст правильного слова
                callback_data=f"keyboard_learning_check|{words[0][1]}|{words[0][1]}|{name_list}"
            ),
            types.InlineKeyboardButton(
                text=additional_words[0][1],  # Первое неправильное слово
                callback_data=f"keyboard_learning_check|{additional_words[0][0]}|{words[0][1]}|{name_list}"
            )
        ],
        [
            types.InlineKeyboardButton(
                text=additional_words[1][1],  # Второе неправильное слово
                callback_data=f"keyboard_learning_check|{additional_words[1][0]}|{words[0][1]}|{name_list}"
            ),
            types.InlineKeyboardButton(
                text=additional_words[2][1],  # Третье неправильное слово
                callback_data=f"keyboard_learning_check|{additional_words[2][0]}|{words[0][1]}|{name_list}"
            )
        ]
    ]

    # Перемешиваем все кнопки, кроме кнопки для удаления
    all_buttons = [button for row in buttons for button in row]  # Все кнопки в один список
    random.shuffle(all_buttons)  # Перемешиваем кнопки

    # Добавляем кнопку корзины в конец списка
    delete_button = types.InlineKeyboardButton(
        text="🗑",  # Кнопка для удаления
        callback_data=f"keyboard_delete_word|{words[0][1]}|{name_list}"
    )

    # Разбиваем обратно на строки
    shuffled_buttons = [
        all_buttons[:2],  # Первая строка с двумя кнопками
        all_buttons[2:4],  # Вторая строка с двумя кнопками
        all_buttons[4:] + [delete_button]  # Третья строка с кнопкой удаления
    ]

    # Создаем клавиатуру с перемешанными кнопками
    keyboard_learning_return = types.InlineKeyboardMarkup(inline_keyboard=shuffled_buttons)

    # Отправляем сообщение пользователю
    await message.answer(
        f"{name_list} Как переводится слово: {words[0][2]}?",  # Выводим слово для перевода
        reply_markup=keyboard_learning_return
    )

# ###################### Обработка проверки слова #####################
@router.callback_query(lambda cb: cb.data.startswith("keyboard_learning_check"))
async def keyboard_learning_check_fun1(query: types.CallbackQuery):
    # Извлечение данных из callback_data
    _, selected_word_id, correct_word_id, name_list = query.data.split('|')
    # Проверка правильности перевода
    if selected_word_id == correct_word_id:
        await query.answer("Верно 👍")
    else:
        await query.answer("Не верно 👎")
    
    # Переход к следующему слову для обучения
    await words_learnings_return_main(query.message,query.from_user.id,name_list)

# ###################### Обработка удаления слова #####################
@router.callback_query(lambda cb: cb.data.startswith("keyboard_delete_word"))
async def delete_word_from_list(query: types.CallbackQuery):
    try:
        # Распаковываем данные из callback_data

        _, word_en, name_list = query.data.split('|')
        user_id = query.from_user.id  # Получаем ID пользователя из сообщения

        # Получаем ID слова

        word_en_id = SQLiteHandler.get_word_en_id(word_en)
        if not word_en_id:
            await query.answer("Ошибка: слово не найдено в базе.")
            return

        # Удаляем слово из списка (или добавляем, если не было)
        action = SQLiteHandler.update_user_list(user_id, word_en_id, name_list)
        if not action:
            await query.answer("Произошла ошибка при обновлении списка.")
            return

        # Получаем обновленный список слов
        learning_words = SQLiteHandler.get_user_words(user_id, name_list)

        # Уведомляем пользователя об успешном удалении
        await query.answer(f"Слово успешно {action} из списка {name_list}.")

        # Обновляем интерфейс и страницу Telegraph
        await words_learnings_return_main(query.message,query.from_user.id,name_list)
        TelegraphHandler.create_or_update_telegraph_page(user_id, name_list)

    except ValueError as e:
        # Обработка ошибок при распаковке данных из callback_data
        await query.answer("Неверный формат данных.")
        print(f"ValueError: {e}")

    except Exception as e:
        # Общая обработка ошибок
        await query.answer("Произошла ошибка при удалении слова.")
        print(f"Unexpected error: {e}")


# Регистрация роутера в диспетчере
dp.include_router(router)

# Запуск бота
def main():
    # Создание базы данных, если она не существует
    SQLiteHandler.create_database_if_not_exists()
    
    # Запуск polling
    dp.run_polling(bot)

if __name__ == "__main__":
    main()
