import os
import requests
from html import escape
from database import SQLiteHandler

API_TOKEN_TELEGRAPH = os.getenv('API_TOKEN_TELEGRAPH')
class TelegraphHandler:
    def __init__(self):
        self.access_token = API_TOKEN_TELEGRAPH

    def get_page_list(self, user_id,name_list):
        try:
            api_url = "https://api.telegra.ph/getPageList"
            title= f"{name_list} Words for User {user_id}"
            params = {"access_token": self.access_token, "limit": 10}
            response = requests.get(api_url, params=params)
            result = response.json()
            if result["ok"]:
                pages = result["result"]["pages"]
                for page in pages:
                    if page["title"] == title:
                        url = page["url"]
                        path = page["path"]
                        return url, path  
            return None,None
        except requests.Error as e:
            print(e)

    def create_or_update_telegraph_page(self, user_id, list_name):
        # Получаем слова пользователя для указанного списка
        lists_words = SQLiteHandler().get_user_words(user_id, list_name)
        if not lists_words:
            print(f"Список {list_name} пользователя {user_id} пуст. Страница не обновлена.")
            return None

        title = f"{list_name} Words for User {user_id}"
        _, path = self.get_page_list(user_id, list_name)
        content = []

        # Формируем контент в формате "слово - перевод"
        for word_en_id, word_en, translations_str in lists_words:
            row = f"{escape(word_en)} - {escape(translations_str)}"
            content.append({"tag": "p", "children": [row]})

        # Формируем параметры для Telegraph API
        params = {
            "access_token": self.access_token,
            "title": title,
            "content": content,
            "return_content": True
        }

        if path:  # Обновляем существующую страницу
            api_url = f"https://api.telegra.ph/editPage/{path}"
        else:  # Создаем новую страницу
            api_url = "https://api.telegra.ph/createPage"

        response = requests.post(api_url, json=params)
        result = response.json()

        if result["ok"]:
            updated_url = result["result"]["url"]
            print(f"Страница {'обновлена' if path else 'создана'}: {updated_url}")
            return updated_url
        else:
            print(f"Ошибка при {'обновлении' if path else 'создании'} страницы:", result["error"])
            return None
